# IDS721_mini_project4

# Overview：
To containerize a Rust Actix Web Service, including containerizing a simple Rust Actix web app, building Docker image, and running container locally.

# Steps:
1.	Open cmd on Windows and use "cargo new my_actix_app" to create a new Rust project.
2.	Go to /src/main.rs and create a function that generates two random numbers and adds them
together to create a web app. The code is shown below.

![Alt text](c575a9d97cdac7d8190d80c4830332e.png)

3.	In Cargo. toml, it should include the following.
'''
[dependencies]
actix-web = "4"
rand = "0.8"
'''
4.	Create a corresponding Dockerfile like the following.

![Alt text](9ca7a9faa3582e59f61a8b172bcf105-1.png)

5.	Open the command prompt and navigate to the current directory. Then, run the command 'docker build -t my_actix_app .'. 
Finally, execute the command 'docker run -d -p 8080:8080 my_actix_app'. Then, open Docker Desktop and navigate to the 'Containers' section. I can be able to run the container named 'determined_mestorf'.  

![Alt text](f7cdcebeb5187e3dfcbcdbefba73a75-1.png)

6.	I can see my web app under http://localhost:8080/ like the following.

![Alt text](cf0318e9e12208b7b6bb9626cd631b4.png)
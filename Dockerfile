# 使用官方Rust镜像作为基础镜像
FROM rust:latest as builder

# 创建应用目录
WORKDIR /usr/src/my_actix_app

# 将当前目录内容复制到容器中
COPY . .

# 为发布构建你的程序
RUN cargo build --release

# 使用Debian作为运行时环境
FROM debian:bookworm-slim


# 从构建器阶段复制构建产物
COPY --from=builder /usr/src/my_actix_app/target/release/my_actix_app .

# 安装所需的包
RUN apt-get update \
    && apt-get install -y ca-certificates \
    && rm -rf /var/lib/apt/lists/*

# 向外界暴露8080端口
EXPOSE 8080

# 运行可执行文件的命令
CMD ["./my_actix_app"]
